/*let address = {
	city: "Imus",
	province: "Cavite",
	country: "Philippines"
};
console.log(address);*/


// DIFFERENCE
/*
1) Quotation marks
JS Objects - for Values only
JSON - for Keys and Values

2) Use
JS Objects - exclusive to Javascript only
JSON - can be used by other prgrammign languages
*/

// JSON Object
/*
	JSON: Javascript Object Notation
	JSON - used for serializing/deserializing different data types into byte
		Serialization - process of converting data into series of bytes for easier transmission or transfer of information
*/
/*let address = {
	"city": "Ormoc",
	"province": "Leyte",
	"country": "Philippines"
};
console.log(address);*/

// JSON Arrays
/*let cities = [
{
	"city": "Ormoc",
	"province": "Leyte",
	"country": "Philippines"
},
{
	"city": "Bacoor",
	"province": "Cavite",
	"country": "Philippines"
},
{
	"city": "New York",
	"province": "New York",
	"country": "USA"
}
];
console.log(cities)*/

/*JSON METHODS
	- JSON object contatins methods for parsing and converting data into stringified JSON
	- Stringified JSON - JSON Object converted into string to be used in other functions of the language esp. Javascript-based applications (serialize)

*/

let batches = [
{
 "batchName": "Batch X"
},
{
 "batchName": "Batch Y"
}
];
console.log("Result from console.log method");
console.log(batches);

// STRINGIFY - used to convert JS/JSON Objects into JSON (string)
console.log("Result from stringify method");
console.log(JSON.stringify(batches));

let data = JSON.stringify({
	name: "John",
	age: 31,
	address:{
		city: "Manila",
		country: "Philippines"
	}
})
console.log(data);

/*
Assignment
	create userDetails variable that will contain JS object with the following properties:
	fname - prompt
	lname - prompt
	age - prompt
	address:{
		city - prompt
		country - prompt
		zipCode - prompt
	}
	
	log in the console the converted JSON data type
*/

/*let userDetails = JSON.stringify(
{
	fname:prompt(`Please input your first name.`),
	lname:prompt(`Please input your last name`),
	age:prompt(`Please  input your age`),
	address: {
		city: prompt(`City`),
		country: prompt(`Country`),
		zipCode: prompt(`Zipcode`)
	}
}
);
console.log(userDetails)*/


/*
	Converting of stringified JSON into JS Objects
		
		Parese method - converting json data into js objects
		information is commonly sent to application in STRINGIFIED JSON; then converted into objects; this happens both for sending information to a backend app such as database and back to frontend app such as the webpages

		Upon receiving the data, JSON text can be converted into a JS/JSON Object with parse method

*/
let batchesJSON = `[
{
	"batchName": "Batch X"
},
{
	"batchName": "Batch Y"
}
]`;
console.log(batchesJSON);
console.log("Result from parse method:");
console.log(JSON.parse(batchesJSON));

let stringifiedData = `{
	"name": "John",
	"age": "31",
	"address":{
		"city": "Manila",
		"country": "Philippines"
	}
}`
console.log(JSON.parse(stringifiedData))